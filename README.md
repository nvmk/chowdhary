# Chowdhary

Select the project which you have permissions to access Compute Engine, Cloud Pub/Sub, Cloud Functions and Cloud Scheduler. Remember to enable these APIs first.

$ cloudshell_open --repo_url "https://github.com/browny/gcp-cloud-scheduler-tutorial" --page "shell" --tutorial "README.md" 

# My name is manikumar chowdhary
# the below is the schedule start and stop of instances in gcp
# Better dont come in my way
Set up the Compute Engine instance

Create a sample instance with label env=dev which will be used as filter when start/stop action executed by Cloud Functions.

gcloud compute instances create dev-instance \
  --network default \
  --zone us-west1-b \
  --labels=env=dev

Set up the Cloud Functions functions with Pub/Sub

Create 2 Cloud Pub/Sub topics which will be used as the triggers of following Cloud Functions.
1. Create start-instance and stop-instance event

gcloud pubsub topics create start-instance-event

gcloud pubsub topics create stop-instance-event

Create 2 Cloud Funtions, one for starting instances, the other for stopping instances. They are triggered by the message of above 2 Cloud Pub/Sub topics.
2. Get code

git clone https://github.com/GoogleCloudPlatform/nodejs-docs-samples.git

cd nodejs-docs-samples/functions/scheduleinstance/

3. Create the start and stop functions

gcloud functions deploy startInstancePubSub \
  --trigger-topic start-instance-event \
  --runtime nodejs8 \
  --allow-unauthenticated

gcloud functions deploy stopInstancePubSub \
  --trigger-topic stop-instance-event \
  --runtime nodejs8 \
  --allow-unauthenticated

Once the functions created, we can manually trigger it to verify the instance being stopped or started.
4. (Optional) Verify the functions work

gcloud functions call stopInstancePubSub \
--data '{"data":"eyJ6b25lIjoidXMtd2VzdDEtYiIsICJsYWJlbCI6ImVudj1kZXYifQo="}'

gcloud compute instances describe dev-instance \
--zone us-west1-b \
| grep status

Set up the Cloud Scheduler jobs to call Pub/Sub

Now we have 2 Cloud Functions with corresponding Cloud Pub/Sub topics as their triggers ready. Then we will create 2 Cloud Scheduler jobs, one for starting the instance at 9AM every weekday (Mon. to Fri.), the other for stopping the instance at 10AM every weekday.
1. Create the jobs (notice: change below --schedule parameter to 0 9 * * 1-5 / 0 10 * * 1-5)

gcloud beta scheduler jobs create pubsub startup-dev-instances \
  --schedule '0 9 * * 1-5' \
  --topic start-instance-event \
  --message-body '{"zone":"us-west1-b", "label":"env=dev"}' \
  --time-zone 'Asia/Taipei'

gcloud beta scheduler jobs create pubsub shutdown-dev-instances \
  --schedule '0 10 * * 1-5' \
  --topic stop-instance-event \
  --message-body '{"zone":"us-west1-b", "label":"env=dev"}' \
  --time-zone 'Asia/Taipei'

Once the jobs created, we don't need to wait until the scheduled time, we can manaully run the jobs to verify the following actions being ran accordingly.
2. (Optional) Verify the jobs work

gcloud beta scheduler jobs run shutdown-dev-instances

gcloud compute instances describe dev-instance \
--zone us-west1-b \
| grep status

gcloud beta scheduler jobs run startup-dev-instances

gcloud compute instances describe dev-instance \
--zone us-west1-b \
| grep status
